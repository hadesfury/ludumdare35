﻿using UnityEngine;
using System.Collections;

public class TrucFearTrigger : MonoBehaviour {

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "TRUC" )//&& other.attachedRigidbody.velocity.magnitude > 0f )
        {
            TrucController tc = other.transform.GetComponent<TrucController>();
            if (tc.state != TrucController.TrucStates.PANIC)
            {
                tc.Settle(transform.position);
            }
           
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "TRUC")
        {
            other.transform.GetComponent<TrucController>().StopPanic(transform.position);
        }
    }
}
