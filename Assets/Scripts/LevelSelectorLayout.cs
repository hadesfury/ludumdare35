﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelectorLayout : MonoBehaviour
{
    public Text levelSceneNameText;
    public Text levelBestScoreText;
    public Button selectLevelButton;

    private string levelSceneName;

    void Awake()
    {
        levelBestScoreText.gameObject.SetActive( false );
    }

    public void SetLevelSceneName( string par_level_scene_name )
    {
        levelSceneName = par_level_scene_name;

        levelSceneNameText.text = levelSceneName;

        string loc_current_level_best_score_id = par_level_scene_name.Replace( ' ', '-' ) +"_BestScore";

        if( PlayerPrefs.HasKey( loc_current_level_best_score_id ) )
        {
            levelBestScoreText.gameObject.SetActive( true );
            levelBestScoreText.text = "Best : " + PlayerPrefs.GetInt( loc_current_level_best_score_id );
        }
    }

    public void OnLevelSelectedButton()
    {
        SceneManager.LoadScene( levelSceneName );
        //GameManager.gameManager.OnResume();
    }
}
