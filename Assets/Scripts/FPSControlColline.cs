﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FPSControlColline : MonoBehaviour
{
    public float
        speed = 10.0f,
        runSpeed = 15.0f,
        rotationSpeed = 0.5f,
        minimumY = -60F,
        maximumY = 60F;

    [Header( "Sounds" )]
    public List<AudioClip> runClipTable;

    private Camera 
        cam;

    private Vector3
        horiz,
        vert,
        direction;


    // Use this for initialization
    void Start()
    {
        cam = GetComponentInChildren<Camera>();
    }

    void Update()
    {
        if( Input.GetButtonDown( "Launch Stone" ) )
        {
            GameManager.gameManager.InstatiateStone();
        }
    }

    void FixedUpdate()
    {
        float padHoriz = Input.GetAxis( "Horizontal" );
        float padVert = Input.GetAxis( "Vertical" );


        vert = cam.transform.forward;
        vert.y = 0;
        horiz = cam.transform.right;

        direction = ( horiz * padHoriz + vert.normalized * padVert ) * 0.5f * speed;
        Debug.DrawRay( cam.transform.position, direction, Color.red, .1f );
        direction *= Time.deltaTime;

        float rotationX = cam.transform.localEulerAngles.x;
        rotationX += Input.GetAxis( "RightStickVertical" ) * rotationSpeed * Time.deltaTime;

        if( rotationX < 200 )
        {
            rotationX = Mathf.Min( rotationX, maximumY );
        }
        else
        {
            rotationX = Mathf.Max( rotationX, minimumY );
        }

        cam.transform.localEulerAngles = new Vector3( rotationX, 0, 0 );

        transform.Rotate( 0, Input.GetAxis( "RightStickHorizontal" ) * rotationSpeed * Time.deltaTime, 0, Space.World );


        transform.Translate( direction, Space.World );

    }
}