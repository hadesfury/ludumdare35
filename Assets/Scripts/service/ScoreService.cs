﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScoreService
{
#if UNITY_EDITOR
    private static string SCORES_URL    = "http://ludaes-ludumdare32.rhcloud.com/ld35-score-dev/score";
    private static string SAVES_URL     = "http://ludaes-ludumdare32.rhcloud.com/ld35-saves-dev/saves";
#endif
#if !UNITY_EDITOR
    private static string SCORES_URL = "http://ludaes-ludumdare32.rhcloud.com/ld35-score/score";
    private static string SAVES_URL = "http://ludaes-ludumdare32.rhcloud.com/ld35-saves/saves";
#endif

    public IEnumerator RegisterScore( Score score, Action<bool> par_on_complete_callback )
    {
        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        headers.Add("X-HTTP-Method-Override", "POST");

        string json = JsonUtility.ToJson(score);

        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(json);
        WWW www = new WWW(SCORES_URL, bytes, headers);

        yield return www;

        // check for errors
        if (www.error != null)
        {
            Debug.Log( "Error while registering score: " + www.error );
            par_on_complete_callback( false );
        }
        else
        {
            IndexResult result = JsonUtility.FromJson<IndexResult>(www.text);
            if (!result.created)
            {
                Debug.Log( "Error while registering score: " + www.text );
                par_on_complete_callback( false );
            }
            else
            {
                Debug.Log("Score registered with id: " + result._id + " under version: " + result._version);

                par_on_complete_callback( true );
            }            
        }
    }

    public IEnumerator GetHighestScore(string who, string levelId, Action<List<Score>, Score> callback, Score par_current_score = null)
    {
        List<Score> scores = new List<Score>();

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application /json");
        headers.Add("X-HTTP-Method-Override", "GET");

        //byte[] bytes = System.Text.Encoding.UTF8.GetBytes(json);
        string query_string = "/_search?size=20&sort=score:desc";
        string query_parameter = "&q=";
        if (who != null)
        {
            if (query_parameter != "&q=")
            {
                query_parameter = query_parameter + "%20AND%20";
            }
            query_parameter = query_parameter + "who:" + who;
        }
        if (levelId != null)
        {
            if (query_parameter != "&q=")
            {
                query_parameter = query_parameter + "%20AND%20";
            }
            query_parameter = query_parameter + "levelId:\""  + levelId + "\"";
        }

        if (query_parameter != "&q=")
        {
            query_string = query_string + query_parameter;
        }

        Debug.Log("Perform query: " + SCORES_URL + query_string);

        WWW www = new WWW(SCORES_URL + query_string, null, headers);

        yield return www;

        // check for errors
        if (www.error != null)
        {
            Debug.Log("Error while retrieving highest scores" + www.error);
        }
        else
        {
            ScoreQueryResult result = JsonUtility.FromJson<ScoreQueryResult>(www.text);
            int numberOfResults = result.hits.hits.Count;
            Debug.Log("Highest Scores retrieved: " + numberOfResults);
            foreach(var entry in result.hits.hits)
            {
                scores.Add(entry._source);
            }
            callback(scores, par_current_score);
        }
    }
}

[System.Serializable]
public class IndexResult
{
    // {"_index":"ld35-score-dev","_type":"score","_id":"AVQgmQzKqK6Mecpa_vTC","_version":1,"created":true}

    public string _index;
    public string _type;
    public string _id;
    public long _version;
    public bool created;
}

[System.Serializable]
public class ScoreQueryResult
{
    public long took;
    public bool timed_out;
    public Shards _shards;
    public Hits hits;

    [System.Serializable]
    public class Shards
    {
        public long total;
        public long successful;
        public long failed;
    }

    [System.Serializable]
    public class Hits
    {
        public long total;
        public double max_score;
        public List<HitsEntry> hits;
    }

    [System.Serializable]
    public class HitsEntry
    {
        public string _index;
        public string _type;
        public string _id;
        public double _score;
        public Score _source;
    }
}

/*
{
	"took": 2,
	"timed_out": false,
	"_shards": {
		"total": 5,
		"successful": 5,
		"failed": 0
	},
	"hits": {
		"total": 400,
		"max_score": 1.0,
		"hits": [{
			"_index": "ld35-score-dev",
			"_type": "score",
			"_id": "AVQg17fXqK6Mecpa_vgK",
			"_score": 1.0,
			"_source": {
				"score": 4579,
				"who": "DeepTerror",
				"where": ""
			}
		}]
	}
}
*/
