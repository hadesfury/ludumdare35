﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Rendering;

public class StoneShapeLayout : MonoBehaviour 
{
    public List<Collider> colliderTable;
    public MeshFilter mesh;
    public Material debugShapeMaterial;

    public void ConfigureAsMapObject( Transform par_dock )
    {
        foreach( Collider loc_collider in colliderTable )
        {
            GameObject.Destroy( loc_collider.gameObject );
        }

        CommonConfiguration( par_dock );
    }

    public void ConfigureAsShapeObject( Transform par_dock )
    {
        foreach( Collider loc_collider in colliderTable )
        {
            loc_collider.gameObject.name = "TargetShape";
            loc_collider.isTrigger = true;
        }

        CommonConfiguration( par_dock );

        mesh.gameObject.layer = LayerMask.NameToLayer( "Shape" );

        MeshRenderer loc_mesh_renderer = mesh.GetComponent<MeshRenderer>();
        //loc_mesh_renderer.enabled = false;
        loc_mesh_renderer.material = debugShapeMaterial;
    }

    private void CommonConfiguration( Transform par_dock )
    {
        MeshRenderer loc_mesh_renderer = mesh.GetComponent<MeshRenderer>();
        //Color loc_current_color = loc_mesh_renderer.material.color;

        //loc_current_color.a = 0.5f;

        //loc_mesh_renderer.material.color = loc_current_color;

        loc_mesh_renderer.shadowCastingMode = ShadowCastingMode.Off;
        loc_mesh_renderer.receiveShadows = false;

        GameObject.Destroy( mesh.GetComponent<Rigidbody>() );
        mesh.transform.SetParent( par_dock, false );
        // Reset transform component
        //mesh.transform.localPosition = Vector3.zero;
        //mesh.transform.localScale = Vector3.zero;
        //mesh.transform.localRotation = Quaternion.identity;

        GameObject.Destroy( gameObject );
    }
}
