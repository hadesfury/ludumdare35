﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[Serializable]
public class Score
{
    public long score;
    public string levelId;
    public string when;
    public string who;
    public string where;

    public Score(string who, string where, long score, string levelId)
    {
        this.who = who;
        this.where = where;
        this.score = score;
        this.levelId = levelId;
        this.when = DateTime.UtcNow.ToString("yyyy-MM-dd'T'HH:mm:ss.fff");
    }

    public Score(string who, string where, long score, string levelId, DateTime when) : this(who, where, score, levelId)
    {
        this.when = when.ToString("yyyy-MM-dd'T'HH:mm:ss.fff");
    }
}