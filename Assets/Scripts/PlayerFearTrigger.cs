﻿using UnityEngine;
using System.Collections;

public class PlayerFearTrigger : MonoBehaviour {

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "TRUC")
        {
            other.transform.GetComponent<TrucController>().Panic(transform.position);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "TRUC")
        {
            other.transform.GetComponent<TrucController>().StopPanic(transform.position);
        }
    }
}
