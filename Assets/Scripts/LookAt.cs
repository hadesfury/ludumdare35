﻿using UnityEngine;
using System.Collections;

public class LookAt : MonoBehaviour
{
    public Transform target;
    public float
        weight = 1,
        speed = 0.1f;
        
    void Update()
    {
        transform.rotation = Quaternion.LookRotation( Vector3.Lerp(
            transform.forward,
            Vector3.Lerp( transform.parent.forward, target.position - transform.position, weight ),
            speed ) );
    }
}
