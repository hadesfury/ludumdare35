﻿using UnityEngine;
using System.Collections;

public class WallFearCollider : MonoBehaviour {

    void OnCollisionEnter(Collision collision)
    {
        GameObject gao = collision.collider.gameObject;
        if (gao.tag == "TRUC")
        {
            ContactPoint contact = collision.contacts[0];
            gao.GetComponent<TrucController>().Panic(gao.transform.position * 2);
        }
    }

    void OnCollisionExit(Collision collision)
    {
        GameObject gao = collision.collider.gameObject;
        if (gao.tag == "TRUC")
        {
            StartCoroutine(WaitAndStopPanic( gao.GetComponent<TrucController>() ));
        }
    }
    IEnumerator WaitAndStopPanic (TrucController ctrl)
    {
        yield return new WaitForSeconds(0.5f);
        ctrl.StopPanic(Vector3.zero);
    }
}
