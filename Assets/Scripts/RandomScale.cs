﻿using UnityEngine;
using System.Collections;

public class RandomScale : MonoBehaviour {

    public float min = 0.8f, max = 1.2f;

	// Use this for initialization
	void Start ()
    {
        float r = Random.Range(min, max);
        transform.localScale += new Vector3(r, r, r);
	}
}
