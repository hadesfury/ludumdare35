﻿using UnityEngine;
using System.Collections;

public class SheepSpawner : MonoBehaviour
{
    public int sheepCount = 128;
    public int angleRange = 45;
    public int minimumSpeed = 500;
    public int maximumSpeed = 2000;
    public float timeBetweenSpawn = 0.2f;
    public Transform sheepPrefab;
    public Transform sheepDock;
    public Transform meshHelper;

    void Awake()
    {
        meshHelper.gameObject.SetActive( GameManager.isDebugModeEnabled );    
    }

    public int GetInitialAngle()
    {
        return ( int ) transform.rotation.eulerAngles.y;
    }

    public Vector3 GetSpawnPosition()
    {
        return transform.position;
    }

    public IEnumerator ThrowSheeps()
    {
        int loc_initial_angle = GetInitialAngle();
        int loc_angle_range = Mathf.Clamp( Mathf.Abs( angleRange ), 0, 180 );
        Vector3 loc_spawn_position = GetSpawnPosition();
        int loc_minimum_speed = minimumSpeed;
        int loc_maximum_speed = maximumSpeed;
        float loc_time_between_spawm = timeBetweenSpawn;
        int loc_current_throw_angle = loc_initial_angle;

        for( int sheep_index = 0; sheep_index < sheepCount; sheep_index++ )
        {
            Transform loc_current_sheep = GameObject.Instantiate( sheepPrefab );

            loc_current_sheep.transform.SetParent( sheepDock, false );

            loc_current_sheep.position = loc_spawn_position;

            Vector3 loc_throw_vector = new Vector3( Mathf.Sin( loc_current_throw_angle * Mathf.Deg2Rad ), 0.01f, Mathf.Cos( loc_current_throw_angle * Mathf.Deg2Rad ) ).normalized;

            loc_throw_vector *= loc_minimum_speed + Mathf.RoundToInt( Random.Range( 0.0f, 1.0f ) * ( loc_maximum_speed - loc_minimum_speed ) );
            loc_current_sheep.GetComponent<Rigidbody>().AddForce( loc_throw_vector );

            yield return new WaitForSeconds( loc_time_between_spawm );

            loc_current_throw_angle = loc_initial_angle + Mathf.RoundToInt( Random.Range( -1.0f, 1.0f ) * loc_angle_range );
        }
    }

    //private IEnumerator GenerateSheep( int par_sheep_count )
    //{
    //    int current_batch_count = par_sheep_count / 16;
    //    int current_batch_index = 0;
    //    int current_batch_radius = sheepRadius / 8;

    //    float current_sheep_angle = ( 2 * Mathf.PI ) / current_batch_count;

    //    for( int sheep_index = 0; sheep_index < par_sheep_count; sheep_index++ )
    //    {
    //        Transform current_sheep = GameObject.Instantiate( sheepPrefab );

    //        current_sheep.transform.SetParent( sheepDock, false );

    //        current_sheep.localPosition = new Vector3( Mathf.Sin( current_sheep_angle * sheep_index ), 0, Mathf.Cos( current_sheep_angle * sheep_index ) ).normalized * current_batch_radius;

    //        yield return null;
    //        ++current_batch_index;

    //        if( current_batch_index >= current_batch_count )
    //        {
    //            current_batch_index = 0;
    //            current_batch_count = Mathf.FloorToInt( current_batch_count * 1.3f );
    //            current_batch_radius = Mathf.FloorToInt( current_batch_radius * 1.3f );
    //            current_sheep_angle = ( 2 * Mathf.PI ) / current_batch_count;
    //        }
    //    }
    //}
}
