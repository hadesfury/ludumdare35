﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FPSControl : MonoBehaviour
{

    public float
        grabDistance = 5,
        grabThrowForce = 1000,
        speed = 10.0f,
        runSpeed = 15.0f,
        rotationSpeed = 0.5f,
        fadeTime = .25f,
        //		jumpMult = 2f,
        //		jumpCoolDown = .5f,
        minimumY = -60F,
        maximumY = 60F,
        fearRadiusMultiplier = 2f;

    [Header( "Sounds" )]
    public List<AudioClip> runClipTable;
    public List<AudioClip> moveClipTable;
    public List<AudioClip> shoutClipTable;

    private Camera cam;

    private
        Vector3
            horiz,
            vert,
            direction;
    private string animToPlay;
    private Animator anim;
    private float speedMult;
    //		jumpTimer;

    //	private Rigidbody rb;
    private SphereCollider fearTrigger;

    private GameObject grabbedObject = null;
    private AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        //		Screen.lockCursor = true;
        //		Cursor.visible = false;
        //		Cursor.lockState = CursorLockMode.Locked;
        anim = GetComponentInChildren<Animator>();
        //		anim["idle"].speed = 0.3f;
        //		rb = GetComponent<Rigidbody>();
        //		jumpTimer = 0;
        cam = GetComponentInChildren<Camera>();
        fearTrigger = GetComponentInChildren<SphereCollider>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        //		jumpTimer += Time.deltaTime;

        float runBtn = Input.GetAxis( "Fire1" );
        float padHoriz = Input.GetAxis( "Horizontal" );
        float padVert = Input.GetAxis( "Vertical" );


        if( runBtn == 0 )
        {
            anim.SetBool( "run", false );
            speedMult = speed;
        }
        else
        {
            anim.SetBool( "run", true );
            speedMult = runSpeed;
            if(!anim.GetBool( "shout" ))  SoundManager.PlayFromTable( runClipTable, audioSource );
        }

        vert = cam.transform.forward;
        vert.y = 0;
        horiz = cam.transform.right;

        direction = ( horiz * padHoriz + vert.normalized * padVert ) * 0.5f * speedMult;
        Debug.DrawRay( cam.transform.position, direction, Color.red, .1f );
        direction *= Time.deltaTime;

        float rotationX = cam.transform.localEulerAngles.x;
        rotationX += Input.GetAxis( "RightStickVertical" ) * rotationSpeed * Time.deltaTime;

        if( rotationX < 200 )
            rotationX = Mathf.Min( rotationX, maximumY );
        else
            rotationX = Mathf.Max( rotationX, minimumY );

        cam.transform.localEulerAngles = new Vector3( rotationX, 0, 0 );
        //		cam.transform.localRotation = Quaternion.AngleAxis(rotationX,Vector3.right);

        transform.Rotate( 0, Input.GetAxis( "RightStickHorizontal" ) * rotationSpeed * Time.deltaTime, 0 );

        if( direction.magnitude > 0 )
        {
            anim.SetBool( "move", true );
            if(!anim.GetBool( "shout" )) SoundManager.PlayFromTable( moveClipTable, audioSource );
        }
        else
        {
            anim.SetBool( "move", false );
        }

        transform.Translate( direction, Space.World );

        //		if (Input.GetAxis("Jump")==1 && jumpTimer >= jumpCoolDown)
        //		{
        //			rb.AddForce(Vector3.up * jumpMult);
        //			jumpTimer = 0;
        //		}
    }

    void Update()
    {
        if( Input.GetAxis( "Fire3" ) == 1 )
        {
            fearTrigger.transform.localScale = Vector3.one * fearRadiusMultiplier;
            anim.SetBool( "shout", true );
            SoundManager.PlayFromTable( shoutClipTable, audioSource );
        }
        else
        {
            fearTrigger.transform.localScale = Vector3.one;
            anim.SetBool( "shout", false );
        }

        if( Input.GetButtonDown( "Grab" ) )
        {
            //print( "start grab" );
            Ray loc_ray = new Ray( transform.position, transform.forward );
            RaycastHit[] loc_raycast_hit_table = Physics.RaycastAll( loc_ray, grabDistance );

            if( loc_raycast_hit_table != null )
            {
                GameObject loc_nearest_object = null;
                float loc_nearest_distance = float.MaxValue;
                foreach( RaycastHit loc_raycast_hit in loc_raycast_hit_table )
                {
                    if( ( loc_raycast_hit.collider.GetComponent<ShapeFiller>() != null ) && ( loc_raycast_hit.distance < loc_nearest_distance ) )
                    {
                        loc_nearest_object = loc_raycast_hit.collider.gameObject;
                        loc_nearest_distance = loc_raycast_hit.distance;
                    }
                }

                grabbedObject = loc_nearest_object;
                if( grabbedObject != null )
                {
                    grabbedObject.GetComponent<Rigidbody>().useGravity = false;
                    grabbedObject.transform.SetParent( cam.transform, true );
                }
            }
        }

        if( Input.GetButtonUp( "Grab" ) )
        {
            //print( "release grab" );

            if( grabbedObject != null )
            {
                Rigidbody loc_rigid_body = grabbedObject.GetComponent<Rigidbody>();

                loc_rigid_body.useGravity = true;
                grabbedObject.transform.SetParent( null );

                loc_rigid_body.AddForce( ( loc_rigid_body.transform.position - transform.position ).normalized * grabThrowForce );
            }
        }
    }
}
