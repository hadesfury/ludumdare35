﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class LeaderboardLayout : MonoBehaviour
{
    public Text scoreText;
    public Text playerNameText;
    public Text countryText;
    public Text timeText;

    private static Dictionary<String, byte[]> flags = new Dictionary<string, byte[]>();

    public void SetText( string par_player_name, String where, DateTime par_score_timestamp, long par_score )
    {
        scoreText.text = par_score.ToString().PadLeft( 6 );

        playerNameText.text = par_player_name.PadLeft( 16 );

        timeText.text = par_score_timestamp.ToString("dd/MM/yyyy HH:mm");

        countryText.text = where;
    }
}
