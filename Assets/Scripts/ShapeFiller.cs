﻿using UnityEngine;

public class ShapeFiller : MonoBehaviour
{
    private int isInsideTarget;

    // Use this for initialization
    void Start()
    {
        GameManager.gameManager.Register( this );
    }
    
    void OnTriggerEnter( Collider par_collider )
    {
        if( par_collider.gameObject.name == "TargetShape" )
        {
            ++isInsideTarget;
        }
    }

    void OnTriggerExit( Collider par_collider )
    {
        if( par_collider.gameObject.name == "TargetShape" )
        {
            --isInsideTarget;
        }
    }

    public bool IsInsideTarget()
    {
        return isInsideTarget > 0;
    }
}
