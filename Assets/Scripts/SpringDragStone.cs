﻿using UnityEngine;
using System.Collections;

public class SpringDragStone : MonoBehaviour
{
    public Rigidbody 
        stone;
    public float 
        liftSpeed = 1,
        strengthMultiplier = 1;
    private SpringJoint 
        spring;
    [HideInInspector]
    public bool 
        lift = false;

    void Start()
    {
        spring = GetComponentInChildren<SpringJoint>();
        spring.spring = 0;
    }
    
    void FixedUpdate()
    {
        if( stone.velocity.magnitude == 0)
        {
            if( stone.transform.position.y < 2.0f )
            {
                spring.spring = stone.mass * strengthMultiplier;
                lift = true;
            }
        }

        if( stone.transform.position.y < 10.0f )
        {
            GameManager.gameManager.GameOver();
        }

        if( lift )
        {
            spring.transform.position += Vector3.up * liftSpeed;
        }
        if( stone.transform.localPosition.y > 0 )
        {
            Reset();
        }
    }

    public void Reset()
    {
        spring.spring = 0;
        lift = false;
        stone.transform.localPosition = stone.transform.localEulerAngles = Vector3.zero;
        stone.velocity = stone.angularVelocity = Vector3.zero;
        spring.transform.localPosition = spring.transform.localEulerAngles = Vector3.zero;
    }
}
