﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GuiManager : MonoBehaviour
{
    public RectTransform debugPanelGui;
    public RectTransform losePanelGui;
    public RectTransform winPanelGui;
    public RectTransform pausePanelGui;
    public RectTransform playerControlPanelGui;
    public RectTransform endGameScorePanelGui;

    public LeaderboardManager leaderboardManager;

    public Text levelNameText;
    public Text scoreText;
    public Text timerText;
    public Text godHasBeenCalledMessageText;

    [ Header( "EndGameScore" ) ]
    public Text percentageText;
    public Text timeLeftText;
    public Text totalScoreText;
    public AudioSource countingAudioSource;

    void Awake()
    {
        losePanelGui.gameObject.SetActive( false );
        winPanelGui.gameObject.SetActive( false );
        pausePanelGui.gameObject.SetActive( false );
        playerControlPanelGui.gameObject.SetActive( true );
        endGameScorePanelGui.gameObject.SetActive( false );
        countingAudioSource.gameObject.SetActive( false );
        godHasBeenCalledMessageText.gameObject.SetActive( false );
    }

    void Start()
    {
        debugPanelGui.gameObject.SetActive( GameManager.isDebugModeEnabled );
    }

    public void ShowWinPanel( float par_score, float par_level_time_left )
    {
        winPanelGui.gameObject.SetActive( true );
        losePanelGui.gameObject.SetActive( false );

        //leaderboardManager.DisplayHighscore( SceneManager.GetActiveScene().name );
        StartCoroutine( DisplayEndGameScoreScreen( par_score, par_level_time_left ) );
    }

    public void ShowLosePanel( float par_score, float par_level_time_left )
    {
        losePanelGui.gameObject.SetActive( true );
        winPanelGui.gameObject.SetActive( false );

        //leaderboardManager.DisplayHighscore( SceneManager.GetActiveScene().name );
        StartCoroutine( DisplayEndGameScoreScreen( par_score, par_level_time_left ) );
    }

    public IEnumerator DisplayEndGameScoreScreen( float par_score, float par_level_time_left )
    {
        int loc_score_time = 3;

        endGameScorePanelGui.gameObject.SetActive( true );
        percentageText.text = string.Format( "{0:P}", par_score );
        timeLeftText.text = string.Format( "{0}s",Mathf.RoundToInt(par_level_time_left));
        totalScoreText.text = "0";

        yield return new WaitForSeconds( 2 );

        countingAudioSource.gameObject.SetActive( true );
        StartCoroutine( DisplayCountingText( GameManager.GetComputedCompletionScore( par_score ), percentageText, loc_score_time ) );
        StartCoroutine( DisplayCountingText( GameManager.GetComputedTimeLeftScore( par_score, par_level_time_left ), timeLeftText, loc_score_time ) );
        StartCoroutine( DisplayCountingText( GameManager.GetComputedScore(par_score, par_level_time_left ), totalScoreText, loc_score_time ) );

        yield return new WaitForSeconds( loc_score_time );
        countingAudioSource.gameObject.SetActive( false );
    }

    private IEnumerator DisplayCountingText( int par_final_value, Text par_text_field, int par_count_time )
    {
        float loc_current_value = 0;
        float loc_increase_by_second = par_final_value / ( float ) par_count_time ;

        while( loc_current_value < par_final_value )
        {
            par_text_field.text = Mathf.RoundToInt(loc_current_value).ToString();
            loc_current_value += loc_increase_by_second * Time.deltaTime;
            yield return null;
        }
    }

    public void DisplayScore( float par_score )
    {
        string score_message = string.Format( "Score : {0:P}", par_score );

        scoreText.text = score_message;
    }

    public void DisplayTime( float par_time )
    {
        string time_message = string.Format( "Time Left : {0:00}:{1:00}", Mathf.FloorToInt( par_time / 60.0f ), par_time % 60 );

        timerText.text = time_message;
    }

    public void OnDebugWinButton()
    {
        GameManager.gameManager.GameOverWin();
    }

    public void OnDebugLoseButton()
    {
        GameManager.gameManager.GameOverLose();
    }

    public void OnContinueButton()
    {
        GameManager.gameManager.OnContinue();
    }

    public void OnResumeButton()
    {
        pausePanelGui.gameObject.SetActive( false );
        GameManager.gameManager.OnResume();
        if (GameManager.gameManager.currentCamera != GameManager.gameManager.guyOneCamera)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    public void OnRetryButton()
    {
        GameManager.gameManager.OnRetry();
    }

    public void OnLevelSelectorButton()
    {
        SceneManager.LoadScene( LevelManager.LEVEL_SELECTOR_SCENE_NAME);
    }

    public void OnExitButton()
    {
        SceneManager.LoadScene( LevelManager.MAIN_MENU_SCENE_NAME);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!pausePanelGui.gameObject.activeInHierarchy)
            {
                pausePanelGui.gameObject.SetActive(true);
                GameManager.gameManager.OnPause();
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                OnResumeButton();
            }
        }
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            playerControlPanelGui.gameObject.SetActive(!playerControlPanelGui.gameObject.activeInHierarchy);
        }
    }

    public void DisplayLevelName()
    {
        levelNameText.text = SceneManager.GetActiveScene().name;
    }

    public void DisplayGodHasBeenCalledMessage( int par_stone_fall_time )
    {
        godHasBeenCalledMessageText.gameObject.SetActive( true );
        StartCoroutine( GodCountdownCouroutine(par_stone_fall_time) );
    }

    private IEnumerator GodCountdownCouroutine( int par_stone_fall_time )
    {
        float loc_time_left = par_stone_fall_time;

        while( loc_time_left > 0 )
        {
            godHasBeenCalledMessageText.text = string.Format( "The gods have been called - {0:00}", loc_time_left );
            loc_time_left -= Time.deltaTime;
            yield return null;
        }
    }
}
