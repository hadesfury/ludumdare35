﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager gameManager;
#if UNITY_EDITOR
    public static bool isDebugModeEnabled = true;
#endif
#if !UNITY_EDITOR
    public static bool isDebugModeEnabled = false;
#endif
    [Header( "Shape Docks" )]
    public Transform shapeDock;
    public Transform mapShapeDock;

    [Header( "Level Info" )]
    public string nextLevelName;
    public string decorSceneName = "Decor";
    public float levelTimeDuration = 60 * 3;
    public StoneShapeLayout rockPrefab;
    [Range( 0, 1 )]
    public float levelSuccessThreshold = 0.7f;
    public SheepSpawner sheepSpawner;

    [Header( "Other" )]
    public AudioClip ambientAudioClip;
    public GameObject workSceneToDeactivate;
    public int stoneFallTime = 25;
    public GuiManager guiManager;

    public Camera guyOneCamera;
    public Camera guyTwoCamera;

    public Camera currentCamera;
    private readonly List<ShapeFiller> shapeFillerTable = new List<ShapeFiller>();
    private float levelTimeLeft;
    private float currentScore = 0;
    private bool hasGameEnded = false;
    private bool isStoneInstaciated = false;
    private StoneShapeLayout fallingStone;

    //private readonly List<List<ShapeFiller>> shapeFillerGrid = new List<List<ShapeFiller>>();

    void Awake()
    {
        gameManager = this;
        currentCamera = guyTwoCamera;
        guyOneCamera.GetComponent<AudioListener>().enabled = false;
        guyTwoCamera.GetComponent<AudioListener>().enabled = true;
    }

    void Start()
    {
        levelTimeLeft = levelTimeDuration + stoneFallTime;

        StartCoroutine( sheepSpawner.ThrowSheeps() );

        Time.timeScale = 1;

        guiManager.DisplayLevelName();
        workSceneToDeactivate.SetActive( false );
        SceneManager.LoadSceneAsync( decorSceneName, LoadSceneMode.Additive );

        // Generate rocks
        fallingStone = GameObject.Instantiate( rockPrefab );
        fallingStone.GetComponentInChildren<Rigidbody>().useGravity = false;

        StoneShapeLayout loc_map_stone = GameObject.Instantiate( rockPrefab );
        StoneShapeLayout loc_shape_stone = GameObject.Instantiate( rockPrefab );

        loc_map_stone.ConfigureAsMapObject( mapShapeDock );
        loc_shape_stone.ConfigureAsShapeObject( shapeDock );

        if( SoundManager.soundManager != null )
        {
            SoundManager.soundManager.FadeTrack( ambientAudioClip );
        }

    }

    void Update()
    {
        if( Input.GetButtonDown( "Fire2" ) )
        {
            SwitchCamera();
        }

        ComputeScore();

        if( levelTimeLeft > 0 )
        {
            UpdateTime();
        }
        else
        {
            GameOver();
        }
    }

    public void InstatiateStone()
    {
        if( !isStoneInstaciated )
        {
            fallingStone.GetComponentInChildren<Rigidbody>().useGravity = true;
            isStoneInstaciated = true;
            //levelTimeLeft = stoneFallTime;
            guiManager.DisplayGodHasBeenCalledMessage( stoneFallTime );
        }
    }

    private void UpdateTime()
    {
        guiManager.DisplayTime( levelTimeLeft );

        if( !isStoneInstaciated && ( levelTimeLeft <= stoneFallTime ) )
        {
            GameObject.Instantiate( rockPrefab );
            isStoneInstaciated = true;
        }

        levelTimeLeft -= Time.deltaTime;
    }

    public void GameOverWin()
    {
        guiManager.ShowWinPanel( currentScore, levelTimeLeft );
        LevelManager.UnlockLevel( nextLevelName );
    }

    public void GameOverLose()
    {
        guiManager.ShowLosePanel( currentScore, levelTimeLeft );
    }

    private void ComputeScore()
    {
        if( !hasGameEnded )
        {
            int shape_inside_target_count = 0;

            currentScore = 0;

            foreach( ShapeFiller shape_filler in shapeFillerTable )
            {
                if( shape_filler.IsInsideTarget() )
                {
                    ++shape_inside_target_count;
                }
            }

            currentScore = shape_inside_target_count / ( float ) shapeFillerTable.Count;
        }
        guiManager.DisplayScore( currentScore );
    }

    private void SwitchCamera()
    {
        currentCamera.GetComponent<AudioListener>().enabled = false;
        if( currentCamera == guyOneCamera )
        {
            currentCamera = guyTwoCamera;
            guyOneCamera.gameObject.SetActive( false );
            guyOneCamera.GetComponentInParent<FPSControl>().enabled = false;
            guyTwoCamera.gameObject.SetActive( true );
            guyTwoCamera.GetComponentInParent<FPSControlColline>().enabled = true;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            currentCamera = guyOneCamera;
            guyOneCamera.gameObject.SetActive( true );
            guyOneCamera.GetComponentInParent<FPSControl>().enabled = true;
            guyTwoCamera.gameObject.SetActive( false );
            guyTwoCamera.GetComponentInParent<FPSControlColline>().enabled = false;


            if( !hasGameEnded )
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
        currentCamera.GetComponent<AudioListener>().enabled = true;
    }

    public void Register( ShapeFiller par_shape_filler )
    {
        shapeFillerTable.Add( par_shape_filler );
    }

    public void OnContinue()
    {
        SceneManager.LoadScene( nextLevelName );
    }

    public void OnRetry()
    {
        SceneManager.LoadScene( SceneManager.GetActiveScene().name );
        OnResume();
    }

    public void OnPause()
    {
        Time.timeScale = 0;
    }

    public void OnResume()
    {
        Time.timeScale = 1;
    }

    public void GameOver()
    {
        if( !hasGameEnded )
        {
            hasGameEnded = true;
            if( currentScore > levelSuccessThreshold )
            {
                GameOverWin();
            }
            else
            {
                GameOverLose();
            }
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;

            string loc_player_name = PlayerPrefs.GetString( MainMenuManager.PLAYER_PREFS_NAME_ID );
            string loc_level_name = SceneManager.GetActiveScene().name.Replace( ' ', '-' );
            int loc_score = GameManager.GetComputedScore( currentScore, levelTimeLeft );
            Score loc_player_score = new Score(
                loc_player_name,
                PlayerPrefs.GetString( MainMenuManager.PLAYER_PREFS_COUNTRY_ID ),
                loc_score,
                loc_level_name
                );

            string loc_current_level_best_score_id = loc_level_name + "_BestScore";

            if( !PlayerPrefs.HasKey( loc_current_level_best_score_id ) || ( loc_score > PlayerPrefs.GetInt( loc_current_level_best_score_id ) ) )
            {
                PlayerPrefs.SetInt( loc_current_level_best_score_id, loc_score );
            }

            guiManager.leaderboardManager.DisplayHighscore( loc_level_name, loc_player_score );
            StartCoroutine( new ScoreService().RegisterScore( loc_player_score, OnCompleteCallback ) );
        }
    }

    private void OnCompleteCallback( bool par_does_succeed )
    {

    }

    public static int GetComputedScore( float par_completion, float par_time_left )
    {
        return GetComputedCompletionScore( par_completion ) + GetComputedTimeLeftScore( par_completion, par_time_left );
    }

    public static int GetComputedCompletionScore( float par_completion )
    {
        return Mathf.RoundToInt( par_completion * 10000 );
    }

    public static int GetComputedTimeLeftScore( float par_completion, float par_time_left )
    {
        return Mathf.RoundToInt( par_time_left * par_completion * 10 );
    }
}
