﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MainMenuManager : MonoBehaviour
{
    public const string PLAYER_PREFS_NAME_ID = "PLAYER_NAME_ID";
    public const string PLAYER_PREFS_COUNTRY_ID = "PLAYER_LOCATION_COUNTRY";
    public const string PLAYER_PREFS_IP_ID = "PLAYER_LOCATION_IP";
    public InputField nameInputField;
    public int playerNameSize = 14;

    public AudioClip mainMenuClip;

    void Awake()
    {
        string loc_player_name;

        if( PlayerPrefs.HasKey( PLAYER_PREFS_NAME_ID ) )
        {
            loc_player_name = PlayerPrefs.GetString( PLAYER_PREFS_NAME_ID );
        }
        else
        {
            loc_player_name = SystemInfo.deviceName;
        }

        if( loc_player_name.Length > playerNameSize )
        {
            loc_player_name = loc_player_name.Substring( 0, playerNameSize );
        }

        nameInputField.text = loc_player_name;

        StartCoroutine( FetchPlayerLocationInfo( SetPlayerLocationInfo ) );
    }

    void Start()
    {
        if( SoundManager.soundManager != null )
        {
            SoundManager.soundManager.FadeTrack( mainMenuClip );
        }
    }

    void SetPlayerLocationInfo( PlayerLocationInfo info )
    {
        PlayerPrefs.SetString( PLAYER_PREFS_COUNTRY_ID, info.country );
        PlayerPrefs.SetString( PLAYER_PREFS_IP_ID, info.ip );
    }

    IEnumerator FetchPlayerLocationInfo( System.Action<PlayerLocationInfo> callback )
    {
        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add( "Accept", "application/json; charset=utf-8" );
        headers.Add( "User-Agent", "curl/7.43.0" );
        headers.Add( "X-HTTP-Method-Override", "GET" );

        WWW www = new WWW( "http://ipinfo.io/", null, headers );

        yield return www;

        // check for errors
        if( www.error != null )
        {
            Debug.Log( "Error while player location info: " + www.error );
        }
        else
        {
            PlayerLocationInfo result = JsonUtility.FromJson<PlayerLocationInfo>( www.text );
            callback( result );
        }
    }

    public void OnStartGameButton()
    {
        PlayerPrefs.SetString( PLAYER_PREFS_NAME_ID, nameInputField.text );
        SceneManager.LoadScene( LevelManager.LEVEL_SELECTOR_SCENE_NAME );
    }

    public void OnQuitGameButton()
    {
        Application.Quit();
    }

    public void OnLeaderboardButton()
    {
        SceneManager.LoadScene( LevelManager.LEADERBOARD_SCENE_NAME );
    }
}
