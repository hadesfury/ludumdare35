﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LeaderboardManager : MonoBehaviour
{
    public Color playerScoreColor = Color.magenta;
    public LeaderboardLayout leaderboardLayoutPrefab;
    public RectTransform highscorePanel;
    public RectTransform highscoreEntriesPanel;
    public Text loadingText;

    [Header( "LeaderBoardGeneralMenu" )]
    public RectTransform levelTabBarPanel;
    public Button levelTabButtonPrefab;

    public bool isLeaderboardGeneralMenu;

    //private ScoreService scoreService = new ScoreService();
    private List<LeaderboardLayout> leaderboardLayouts = new List<LeaderboardLayout>();
    private List<Button> levelButtonTable = new List<Button>();
    private string displayedHighscoreLevelName;

    void Awake()
    {
        highscorePanel.gameObject.SetActive( false );

        if( isLeaderboardGeneralMenu )
        {
            foreach( string loc_level_scene_name in LevelManager.levelSceneNameTable )
            {
                Button loc_level_tab_button = GameObject.Instantiate( levelTabButtonPrefab );

                loc_level_tab_button.GetComponentInChildren<Text>().text = loc_level_scene_name;
                string scene_name = loc_level_scene_name;
                loc_level_tab_button.onClick.AddListener( () => OnLevelSelected( scene_name ) );
                loc_level_tab_button.transform.SetParent( levelTabBarPanel, false );
                levelButtonTable.Add( loc_level_tab_button );
            }

            OnLevelSelected( LevelManager.levelSceneNameTable[ 0 ] );
        }
    }


    private void OnLevelSelected( string par_level_name )
    {
        DisplayHighscore( par_level_name );
    }
    //void Start()
    //{
    //    //GenerateScores();

    //    // StartCoroutine(scoreService.GetHighestScore("DeepTerror", "level-1", SetHighestScore));
    //    StartCoroutine(scoreService.GetHighestScore(null, null, SetHighestScore));
    //}

    //void GenerateScores()
    //{
    //    List<string> players = new List<string>() {"DeepTerror", "HadesFury", "GrandBaton", "Mojo", "NeoShaman" };
    //    List<string> locations = new List<string>() { "Belgium", "Canada", "Spain", "Mauritius Islands", "France" };

    //    for (int i = 0; i < 100; i++)
    //    {
    //        StartCoroutine(scoreService.RegisterScore(
    //            new Score(
    //                players[Random.Range(0, players.Count)],
    //                locations[Random.Range(0, locations.Count)],
    //                Random.Range(0, 1000000),
    //                "level-" + Random.Range(1, 11)
    //                )
    //            ));
    //    }
    //}

    public void DisplayHighscore( string par_level_name, Score par_current_score = null)
    {
        foreach (Button loc_level_button in levelButtonTable)
        {
            loc_level_button.interactable = false;
        }

        displayedHighscoreLevelName = par_level_name;

        highscorePanel.gameObject.SetActive( true );
        loadingText.gameObject.SetActive( true );
        loadingText.text = "Loading...";

        ClearLeaderboard();

        par_level_name = par_level_name.Replace( ' ', '-' );

        StartCoroutine( new ScoreService().GetHighestScore( null, par_level_name, WriteHighestScore, par_current_score ) );
    }

    void ClearLeaderboard()
    {
        foreach (LeaderboardLayout leaderboard_layout in leaderboardLayouts)
        {
            GameObject.Destroy(leaderboard_layout.gameObject);
        }

        leaderboardLayouts.Clear();
    }

    void WriteHighestScore( List<Score> scores, Score par_current_score )
    {
        bool loc_is_current_score_inserted = false;

        loadingText.gameObject.SetActive( false );

        ClearLeaderboard();

        foreach ( Score score in scores )
        {
            if( !loc_is_current_score_inserted && ( par_current_score != null ) && ( score.score <= par_current_score.score ) )
            {
                InsertLeaderboardEntry( par_current_score, true );
                loc_is_current_score_inserted = true;
            }
                
            InsertLeaderboardEntry( score );

        }

        if( scores.Count == 0 )
        {
            loadingText.gameObject.SetActive( true );
            loadingText.text = "Empty";
        }
        else
        {
            loadingText.gameObject.SetActive( false );
        }

        if( isLeaderboardGeneralMenu )
        {
            foreach( Button loc_level_button in levelButtonTable )
            {
                if (loc_level_button.GetComponentInChildren<Text>().text != displayedHighscoreLevelName)
                {
                    loc_level_button.interactable = true;
                }                
                else
                {
                    loc_level_button.interactable = false;
                }
            }
        }
    }

    private void InsertLeaderboardEntry( Score par_score, bool par_does_highlight = false)
    {
        LeaderboardLayout leaderboard_layout = GameObject.Instantiate( leaderboardLayoutPrefab );

        leaderboard_layout.transform.SetParent( highscoreEntriesPanel, false );

        if( par_does_highlight || (isLeaderboardGeneralMenu && ( par_score.who == PlayerPrefs.GetString( MainMenuManager.PLAYER_PREFS_NAME_ID ))))
        {
            leaderboard_layout.scoreText.color = playerScoreColor;
            leaderboard_layout.playerNameText.color = playerScoreColor;
            leaderboard_layout.timeText.color = playerScoreColor;
        }

        string loc_player_name = par_score.who.Substring( 0, Mathf.Min( par_score.who.Length, 16 ) );
        DateTime loc_timestamp = Convert.ToDateTime( par_score.when );
        long loc_score = par_score.score;

        leaderboard_layout.SetText( loc_player_name, par_score.where, loc_timestamp, loc_score );
        leaderboardLayouts.Add( leaderboard_layout );
    }

    public void OnBackButton()
    {
        SceneManager.LoadScene( LevelManager.MAIN_MENU_SCENE_NAME );
    }
}
