﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    public static string LEVEL_SELECTOR_SCENE_NAME = "LevelSelector";
    public static string MAIN_MENU_SCENE_NAME = "MainMenu";
    public static string LEADERBOARD_SCENE_NAME = "Leaderboard";

    public RectTransform debugPanelGui;

    public LevelSelectorLayout levelSelectorLayoutPrefab;
    public RectTransform levelPanel;

    public static readonly List<string> levelSceneNameTable = new List<string>()
    {
        "Level 1", 
        "Level 2", 
        "Level 3",
        "Level 4"
    };

    void Start()
    {
        debugPanelGui.gameObject.SetActive( GameManager.isDebugModeEnabled );
        UpdateLevelSelectorUi();
    }

    private void UpdateLevelSelectorUi()
    {
        UnlockLevel( LevelManager.levelSceneNameTable[ 0 ] );

        foreach( string level_scene_name in levelSceneNameTable )
        {
            LevelSelectorLayout level_selector_layout = GameObject.Instantiate( levelSelectorLayoutPrefab );

            level_selector_layout.transform.SetParent( levelPanel, false );
            level_selector_layout.SetLevelSceneName( level_scene_name );

            string level_player_prefs_id = GetLevelPrefsIsLockedId( level_scene_name );

            bool is_level_locked = PlayerPrefs.GetInt( level_player_prefs_id, 1 ) != 0;

            level_selector_layout.selectLevelButton.interactable = !is_level_locked;
        }
    }

    public static string GetLevelPrefsIsLockedId( string par_level_scene_name )
    {
        string level_player_prefs_id = par_level_scene_name + "_isLocked";

        return level_player_prefs_id;
    }

    public static void UnlockLevel( string par_level_scene_name )
    {
        PlayerPrefs.SetInt( GetLevelPrefsIsLockedId( par_level_scene_name ), 0 );
    }

    public void OnBackButton()
    {
        SceneManager.LoadScene( LevelManager.MAIN_MENU_SCENE_NAME );
    }

    public void OnDebugResetProgressionButton()
    {
        PlayerPrefs.DeleteAll();
        OnBackButton();
    }
}
