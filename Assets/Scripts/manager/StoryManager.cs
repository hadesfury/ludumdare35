﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StoryManager : MonoBehaviour
{
    public List<Sprite> storyTable;
    public Image imagePanel;
    public Text skipHintText;

    public AudioClip storyClip;

    private bool canSkip = false;

    void Start()
    {
        if( SoundManager.soundManager != null )
        {
            SoundManager.soundManager.FadeTrack( storyClip );
        }

        StartCoroutine( DisplayStoryCoroutine() );
    }

    private IEnumerator DisplayStoryCoroutine()
    {
        foreach( Sprite loc_story_sprite in storyTable )
        {
            imagePanel.sprite = loc_story_sprite;
            skipHintText.gameObject.SetActive( false );
            canSkip = false;
            yield return new WaitForSeconds( 1 );
            skipHintText.gameObject.SetActive( true );
            canSkip = true;
            while( canSkip )
            {
                yield return null;
            }
        }

        SceneManager.LoadScene( LevelManager.MAIN_MENU_SCENE_NAME );
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(LevelManager.MAIN_MENU_SCENE_NAME);
        }
        else if( canSkip && Input.anyKeyDown )
        {
            canSkip = false;
        }
    }

}
