﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour
{
    public static SoundManager soundManager;

    private AudioSource audioSource;
    private float initialVolume;
    
    void Awake()
    {
        if( SoundManager.soundManager != null )
        {
            GameObject.Destroy( gameObject );
        }
        else
        {
            DontDestroyOnLoad( this );
            soundManager = this;
            audioSource = GetComponent<AudioSource>();
            initialVolume = audioSource.volume;
        }
    }

    public void FadeTrack(AudioClip par_next_clip)
    {
        if( audioSource.clip != par_next_clip )
        {
            StartCoroutine( FadeToTrack( par_next_clip ) );
        }
    }

    private IEnumerator FadeToTrack( AudioClip par_next_clip )
    {
        while( audioSource.volume > 0 )
        {
            audioSource.volume -= Time.deltaTime * 0.1f;
            yield return null;
        }

        audioSource.clip = par_next_clip;

        audioSource.volume = initialVolume;

        audioSource.Play();
    }

    public static void PlayFromTable( List<AudioClip> par_clip_table, AudioSource par_audio_source )
    {
        if( !par_audio_source.isPlaying )
        {
            int loc_random_index = Random.Range( 0, par_clip_table.Count - 1 );

            par_audio_source.PlayOneShot( par_clip_table[ loc_random_index ] );
        }
    }
}
