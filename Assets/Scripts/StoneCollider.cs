﻿using UnityEngine;
using System.Collections;

public class StoneCollider : MonoBehaviour {

    private SpringDragStone spring;

    void Start()
    {
        spring = GetComponentInParent<SpringDragStone>();
    }

    void OnCollisionEnter(Collision collision)
    {
        GameObject other = collision.gameObject;
        if (other.tag == "TRUC" && !spring.lift)
        {
            other.transform.GetComponent<TrucController>().Splat();
        }
    }
}
