﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrucController : MonoBehaviour
{
    public float
        rotationSpeed = 0.1f,
        runSpeed = 1f,
        acceleration = 0.01f,
        decceleration = 0.001f,
        settleMultiplier = 0.1f;

    [Header( "Sounds" )]
    public List<AudioClip> panicClipTable;

    private Vector3
        nullifyY,
        playerPosition0Y,
        position0Y,
        playerDirection;

    private float
        currentAcceleration = 0f,
        speed;

    public enum TrucStates
    {
        IDLE,
        IDLE_WELL_POSITIONED,
        PANIC,
        SETTLING,
        DEAD
    }
    public TrucStates state;

    private Animator anim;
    private AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        nullifyY = new Vector3( 1, 0, 1 );
        anim = GetComponentInChildren<Animator>();
        audioSource = GetComponent<AudioSource>();
        audioSource.pitch = Random.Range(.8f, 1.2f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        position0Y = Vector3.Scale( transform.position, nullifyY );
        playerDirection = position0Y - playerPosition0Y;

        if( state == TrucStates.PANIC )
        {
            transform.forward = Vector3.Lerp( transform.forward, playerDirection, rotationSpeed * Time.deltaTime );
            currentAcceleration = Mathf.Min( currentAcceleration + acceleration, 1f );
        }

        if( state == TrucStates.SETTLING )
        {
            transform.forward = Vector3.Lerp( transform.forward, playerDirection, rotationSpeed * Time.deltaTime * settleMultiplier );
            currentAcceleration = Mathf.Min( currentAcceleration + acceleration * settleMultiplier, 1f * settleMultiplier );
        }

        if( ( state == TrucStates.IDLE ) || ( state == TrucStates.IDLE_WELL_POSITIONED ) )
        {
            currentAcceleration = Mathf.Max( currentAcceleration - decceleration, 0f );
            if( currentAcceleration == 0 )
            {
                transform.forward = Vector3.Lerp( transform.forward, -playerDirection, 0.25f * rotationSpeed * Time.deltaTime );
            }
        }

        if( state == TrucStates.DEAD )
        {
            currentAcceleration = 0;
            anim.enabled = false;
        }

        speed = Mathf.Min( currentAcceleration, runSpeed );

        if( speed > 0 )
        {
            anim.SetBool( "underStone", false );
            anim.SetBool( "move", true );
            transform.Translate( transform.forward * speed, Space.World );
        }
        else
        {
            anim.SetBool( "move", false );

            if( state == TrucStates.IDLE_WELL_POSITIONED )
            {
                anim.SetBool( "underStone", true );
            }
        }
        anim.SetFloat( "speed", speed );

        if( transform.position.y > 1.5f )
        {
            anim.SetBool( "underStone", false );
            anim.SetBool( "fly", true );
        }
        else
        {
            anim.SetBool( "fly", false );

            if( state == TrucStates.IDLE_WELL_POSITIONED )
            {
                anim.SetBool( "underStone", true );
            }
        }
    }

    public void Panic( Vector3 playerPosition )
    {
        state = TrucStates.PANIC;
        playerPosition0Y = Vector3.Scale( playerPosition, nullifyY );
        SoundManager.PlayFromTable( panicClipTable, audioSource );
    }

    public void StopPanic( Vector3 playerPosition )
    {
        state = TrucStates.IDLE;

        if( GetComponent<ShapeFiller>().IsInsideTarget() )
        {
            state = TrucStates.IDLE_WELL_POSITIONED;
        }

        playerPosition0Y = Vector3.Scale( playerPosition, nullifyY );
    }

    public void Splat()
    {
        if( state != TrucStates.DEAD )
        {
            state = TrucStates.DEAD;
            transform.localScale = new Vector3( 1f, 0.2f, 1f );
            transform.position = Vector3.Scale( transform.position, nullifyY );
            GetComponent<Rigidbody>().isKinematic = true;
            foreach( SphereCollider i in GetComponentsInChildren<SphereCollider>() )
            {
                i.enabled = false;
            }
        }
    }

    public void Settle( Vector3 playerPosition )
    {
        state = TrucStates.SETTLING;

        playerPosition0Y = Vector3.Scale( playerPosition, nullifyY );
    }
}
